#===============================================================================
#USAGE : ./keyboard-language.sh language
#DESCRIPTION : Pass as an argument the target language.
#PARAMS: Language has to be the international language code like "us" or "sp"
#TIPS:
#===============================================================================
#
#!/bin/bash
#START

echo "What is the language code you want?\n(br or us)\nOpition:";
read op; 

case $op in
	"us")	echo "The keyboard language changed for us" 
		 localectl set-keymap us;;
	"br") echo "The keyboard language changed for br" 
		 localectl set-keymap br-abnt2;; 
	*) echo "Invalid!" 
	exit 1;;
esac 
#END
#
# Copyright Liquid Galaxy ORG.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
