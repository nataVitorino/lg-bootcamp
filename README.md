![Logo](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSgHNTxiyRuNcLsqtAiRv3RDOscFR-w4FkaHA&usqp=CAU)

# Overview
The "Liquid Galaxy Bootcamp" was created by ex-internships from the "Google Summer of Code" program. It uses concepts of the Liquid Galaxy system in order to help students learn about bash, networks, security, git, etc.

The Bootcamp was designed to introduce students into the open source world and also open source communities. Moreover during the developing of the course you will learn core UNIX and Git concepts from the scratch!






# Important Links
[Liquid Galaxy GitHub](https://github.com/LiquidGalaxyLAB)

[Ubuntu Image](https://releases.ubuntu.com/16.04/)

[Bash Manual](https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html)

[Git Manual](https://git-scm.com/about)

# Before Start

The course is designed to start at any point but we strongly suggest to start for the first milestone (MS). 
Before start, you should Fork the respository and then clone it in your own machine.

Inside every folder you will find a .csv file which contains the issues with all the activities to be solved in each MS. This .csv file should be imported into the Gitlab issues tab.

More information can be found inside the wiki.

# Project Overview
The bootcamp has been structured in different modules or milestones, on which the students will test multiple techniques and learn different capabilities.  

### [MS1 - System Basic Commands](https://gitlab.com/liquid-galaxy/lg-bootcamp/-/wikis/MS1-UNIX-basic-commands)
In this Milestone, your first actions will be focused on understanding basic UNIX commands to perform basic actions that we do via GUI. Bash is the focus of this module and you will learn how to manage with basics concepts like using parameters, conditionals and iterative structures.

### [MS2 - Network Protocols]()
Students will learn Secure Shell (ssh) and Secure Copy Protocol (scp), tools that can be used to access from a local host to a remote host, between remote host and also transfer files between them.

### [MS3 - UNIX + LG]()
After the completition of MS1 the students will update their scripts to fit into the Liquid Galaxy cluster using the knowledge adquired in MS2.

### [MS4 Liquid Galaxy (Part1)]()
The Liquid Galaxy is a cluster of computers in an isolated network that runs severals programs, one of them Google Earth. On this milestone you will learn how to interact with this program.

### [MS5 Liquid Galaxy (Part2)]()
Several projects have been developed since the creation of the Liquid Galaxy ORG. Here you will learn to install and deploy other projects.

### [MS6 Network scripting]()
In this final module you will mix different scripts created in the others milestones to accomplish more complex operations.

## Credits
The projects were developed by Marc Gonzalez and Bruno Faé Faion
On this final module you will find 


